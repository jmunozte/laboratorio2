package com.academiasmoviles.lab2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener{

            val anio = edtAnio.text.toString().toInt()
            var generacion = ""
            var poblacion = ""
            var rasgo = ""


            when (anio) {

                in 1930..1948 -> {
                    generacion = "SILENT GENERATION"
                    poblacion = "6,300,000"
                    rasgo = "Austeridad"
                    imgRasgo.setImageResource(R.drawable.austeridad)

                }
                in 1949..1968 -> {
                    generacion = "BABY BOOM"
                    poblacion = "12,200,000"
                    rasgo = "Ambición"
                    imgRasgo.setImageResource(R.drawable.ambicion)

                }
                in 1969..1980 -> {
                    generacion = "GENERACION X"
                    poblacion = "9,300,000"
                    rasgo = "Obsesión por el éxito"
                    imgRasgo.setImageResource(R.drawable.obsesion)

                }
                in 1981..1993 -> {
                    generacion = "GENERACION Y"
                    poblacion = "7,200,000"
                    rasgo = "Frustración"
                    imgRasgo.setImageResource(R.drawable.frustracion)
                }
                in 1994..2010 -> {
                    generacion = "GENERACION Z"
                    poblacion = "7,800,000"
                    rasgo = "Irreverencia"
                    imgRasgo.setImageResource(R.drawable.irreverencia)
                }
                else -> {
                    generacion = "NO DEFINIDO"
                    poblacion = "NO DEFINIDO"
                    rasgo = "NO DEFINIDO"
                    imgRasgo.setImageResource(R.drawable.ic_launcher_background)
                }

            }

            tvGerneracion.text = generacion
            tvPoblacion.text = poblacion
            tvRasgo.text = rasgo



        }


    }
}